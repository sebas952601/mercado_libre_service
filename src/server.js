/* eslint-disable no-multi-spaces */

const http          = require('http');
const express       = require('express');
const routeConfig   = require('./api/routers');
const expressConfig = require('./config/express');
const config        = require('./config/environment');

const app     = express();
const server  = http.Server(app);

expressConfig(app);
routeConfig(app);

setImmediate(() => {
  server.listen(config.port, () => {
    console.log(`Server listening on port  ${config.port}`);
  });
});

module.exports = app;
