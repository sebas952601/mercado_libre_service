/* eslint-disable max-len */
/* eslint-disable key-spacing */
/* eslint-disable no-multi-spaces */
const { getScript } = require('../../lib/utils');

/**
 * Función para consultar los productos segun el filtro suministrado desde el frontend
 * @param {*} {
 *  search -> Texto el cual se escribio en el header, que nos servira para complementar el endPoint para la consulta
 * }
 */
const products = ({ search }) => new Promise(async (resolve, reject) => {
  try {
    const arrResults  = [];
    const url         = `https://api.mercadolibre.com/sites/MLA/search?q=${search}`;
    const results     = await getScript(url);

    // Con forme a la respuesta que nos da nuestra consulta, armamos un nuevo array de los productos con la información necesaria
    JSON.parse(results).results.forEach((reg) => {
      arrResults.push({
        id:             reg.id,
        title:          reg.title,
        price: {
          decimals:     reg.available_quantity,
          amount:       reg.prices.prices[0].amount,
          currency:     reg.prices.prices[0].currency_id,
        },
        picture:        reg.thumbnail,
        condition:      reg.condition,
        free_shipping:  reg.shipping.free_shipping,
      });
    });

    // Armamos la respuesta que sera devuelta al frontend con los datos que son necesarios
    const regs = {
      author: {
        name:     'Sebastian',
        lastname: 'Saldarriaga Muñoz',
      },
      items:      arrResults,
    };

    resolve({ regs: JSON.stringify(regs) });
  } catch (error) {
    reject({ detail: error });
  }
});

/**
 * Función para consultar la información de un producto segun el id suministrado desde el frontend
 * @param {*} {
 *   id -> Id del producto que nos servira para complementar el endPoint para la consulta
 * }
 */
const product = ({
  id,
}) => new Promise(async (resolve, reject) => {
  try {
    // Consultamos la información del id que se suministró
    const urlId = `https://api.mercadolibre.com/items/${id}`;
    const {
      title,
      price,
      condition,
      currency_id:      currencyId,
      shipping:         freeShipping,
      sold_quantity:    soldQuantity,
      secure_thumbnail: secureThumbnail,
    } = JSON.parse(await getScript(urlId));

    // Para obtener la descripción del producto consultamos dicho id pero, complementamos el endPoint con el parametro description para poder obetener esta
    const urlDescription = `https://api.mercadolibre.com/items/${id}/description`;
    const {
      plain_text: plainText,
    } = JSON.parse(await getScript(urlDescription));

    // Armamos la respuesta que sera devuelta al frontend con los datos que son necesarios de las consultas anteriores
    const regs = {
      author: {
        name:           'Sebastian',
        lastname:       'Saldarriaga Muñoz',
      },
      item: {
        id,
        title,
        price: {
          decimals:     0,
          amount:       price,
          currency:     currencyId,
        },
        condition,
        description:    plainText,
        sold_quantity:  soldQuantity,
        picture:        secureThumbnail,
        free_shipping:  freeShipping.free_shipping,
      },
    };

    resolve({ regs: JSON.stringify(regs) });
  } catch (error) {
    reject({ detail: error });
  }
});

module.exports = {
  product,
  products,
};
