/* eslint-disable no-multi-spaces */
/* eslint-disable key-spacing */
const { Router }  = require('express');
const response    = require('../../lib/response');
const httpCodes   = require('../../lib/httpCodes');
const controller  = require('./products.controller');

const router      = new Router();

router.get('/items', (req, res) => {
  controller.products({ search: req.query.search })
    .then((result) => response.success({
      res,
      message:  result,
      status:   httpCodes.OK,
    }))
    .catch((error) => response.error({
      res,
      ...error,
      prefixDetail: '[ProductsController list]',
    }));
});

router.get('/items/:', (req, res) => {
  controller.product({ id: req.query.id })
    .then((result) => response.success({
      res,
      message:  result,
      status:   httpCodes.OK,
    }))
    .catch((error) => response.error({
      res,
      ...error,
      prefixDetail: '[ProductController detail]',
    }));
});

module.exports = {
  router,
  routeName: 'products',
};
