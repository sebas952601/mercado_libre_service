/* eslint-disable no-multi-spaces */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const { join }                  = require('path');
const { readdirSync, statSync } = require('fs');

const dirs = (p) => readdirSync(p).filter((f) => statSync(join(p, f)).isDirectory());

const routes = dirs('./src/api');

module.exports = (app) => {
  routes.forEach((routeFolder) => {
    const route = require(`./${routeFolder}`);
    app.use(`/${route.routeName}`, route.router);
  });
};
