/* eslint-disable key-spacing */
/* eslint-disable no-multi-spaces */
const cors            = require('cors');
const helmet          = require('helmet');
const logger          = require('morgan');
const express         = require('express');
const passport        = require('passport');
const compression     = require('compression');
const errorHandler    = require('errorhandler');
const methodOverride  = require('method-override');
const config          = require('../config/environment');

module.exports = (app) => {
  const env = app.get('env');
  app.use(compression());
  app.use(express.urlencoded({ extended: false, limit: '50mb' }));
  app.use(express.json({ limit: '50mb' }));
  app.use(methodOverride());
  app.use(logger('dev'));
  app.use(passport.initialize());
  app.use(cors({
    credentials:  true,
    origin:       config.origin,
  }));
  app.use(helmet());
  app.disable('x-powered-by');
  if (env === 'development' || env === 'test') {
    app.use(errorHandler()); // Error handler - has to be last
  }
};
