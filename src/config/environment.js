/* eslint-disable key-spacing */
// eslint-disable-next-line import/no-extraneous-dependencies
require('dotenv').config();

module.exports = {
  env:    process.env.NODE_ENV,
  port:   process.env.PORT || 80,
  origin: process.env.CORS_ORIGIN,
  ip:     process.env.IP || '127.0.0.1',
  secrets: {
    session: 'test-post-sessions',
  },
};
