/* eslint-disable no-multi-spaces */
const http  = require('http');
const https = require('https');

const getScript = (url) => new Promise((resolve, reject) => {
  let client = http;

  if (url.toString().indexOf('https') === 0) client = https;

  client.get(url, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => { data += chunk; });

    resp.on('end', () => { resolve(data); });
  }).on('error', (err) => {
    reject(err);
  });
});

module.exports = { getScript };
