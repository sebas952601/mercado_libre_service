const success = ({
  res, status, message,
}) => {
  console.error(message);
  return res.status(status || 200).json({
    error: '',
    body: message,
  });
};

const error = ({
  res,
  status,
  prefixDetail,
  message,
  detail,
}) => {
  const DETAIL = detail || message;
  console.error(`${prefixDetail} ${DETAIL}`);
  return res.status(status || 500).json({
    error: message || 'Internal error',
    body: '',
  });
};

module.exports = {
  success,
  error,
};
